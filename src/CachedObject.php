<?php
namespace Gungnir\Cache;

/**
 * Simple wrapper object for cache storage
 *
 * @package Gungnir\Cache
 * @author Conny Karlsson <connykarlsson9@gmail.com>
 */
class CachedObject
{
    /** @var mixed The content that should be cached */
	private $data = null;

    /** @var int The timestamp at which this object was cached */
    private $timestamp = null;

    /** @var int The time in seconds that the cache should live */
	private $ttl = null;

    /**
     * Constructor for CachedObject class
     *
     * @param mixed  $data    Any content that should be saved
     * @param integer $ttl    The amount in seconds that cache should live
     */
	public function __construct($data, $ttl = 30)
    {
        $this->data = $data;
        $this->ttl = $ttl;
        $this->timestamp = time();
    }

    /**
     * Returns the TTL(Time to live) that the cached object should live
     *
     * @return int The TTL
     */
    public function getTtl()
    {
        return $this->ttl;
    }

    /**
     * Returns the data associated with this object
     *
     * @return mixed The data that should be stored
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Returns the timestamp of when this object was cached
     *
     * @return int The timestamp
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Checks if cache has expired it's TTL and returns true if
     * it has expired.
     *
     * @return boolean
     */
    public function isExpired()
    { 
        if (time() > $this->getTimestamp() + $this->getTtl()) {
            return true;
        }

        return false;
    }
}
