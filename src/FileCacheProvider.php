<?php
namespace Gungnir\Cache;

/**
 * Simple file cache provider that stores cache on disk
 *
 * @package Gungnir\Cache
 * @author Conny Karlsson <connykarlsson9@gmail.com>
 */
class FileCacheProvider implements CacheProvider
{
    /** @var string Extension of cache files created by the provider */
    private $extension = '.cache';

    /** @var string The base folder which cache files is stored in */
    private $path = ROOT . 'storage/gungnir/cache/';

    /** @var string The base cache key for cache files */
    private $driverCacheKey = 'gungnir-filecache';

    /**
     * {@inheritDoc}
     */
    public function store($data, string $cacheKey)
    {
        $key = $this->buildCacheKey($cacheKey);
        $file = $key . $this->extension;
        if (!is_dir($this->path)) {
            mkdir($this->path, 0777, true);
        }

        $filePath = $this->path . $file;
        $content = serialize(new CachedObject($data));

        return file_put_contents($filePath, $content);
    }

    /**
     * {@inheritDoc}
     */
    public function get(string $cacheKey)
    {
        $key = $this->buildCacheKey($cacheKey);
        $file = $key . $this->extension;
        $filePath = $this->path . $file;
        $content = file_exists($filePath) ? file_get_contents($filePath) : false;
        $cachedObject = empty($content) ?  false : unserialize($content);

        if (empty($cachedObject) || $cachedObject->isExpired()) {
            return false;
        }

        return $cachedObject->getData();
    }

    /**
     * Builds a cache key based on given key and registered cache key
     * for this provirder.
     *
     * @param  string $cacheKey The cache key for a specific cache entry
     * @return string The built cache key
     */
    private function buildCacheKey(string $cacheKey)
    {
        return base64_encode($this->driverCacheKey . '-' . $cacheKey);
    }

}
