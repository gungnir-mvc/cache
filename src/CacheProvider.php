<?php
namespace Gungnir\Cache;

/**
 * Interface to use for different cache providers
 *
 * @package Gungnir\Cache
 * @author Conny Karlsson <connykarlsson9@gmail.com>
 */
interface CacheProvider
{
    /**
     * Get cached data by cache key
     *
     * @param  string $cacheKey The cache key to get data with
     *
     * @return mixed The cached data or false if not found or expired
     */
	public function get(string $cacheKey);

    /**
     * Caches given data under given cache key
     *
     * @param  mixed $data      The data that should be cached
     * @param  string $cacheKey The cache key associated with the data
     *
     * @return boolean          True if cached successfully, else false
     */
	public function store($data, string $cacheKey);
}
